#include "bowling_game.h"
#include <cgreen/cgreen.h>
#include <cgreen/mocks.h>

Describe(BowlingGame);

BeforeEach(BowlingGame)
{ bowling_game_init(); }

AfterEach(BowlingGame)
{ /*Do something*/ }

void roll(const int rolls, const int pins)
{
    for (int i=0; i<rolls; i++)
    {
        bowling_game_roll(pins);
    }
}

Ensure(BowlingGame, test_gutter_game)
{
    roll(20, 0);
    assert_that(bowling_game_score(), is_equal_to(0));
}

Ensure(BowlingGame, test_all_ones)
{
    roll(20, 1);
    assert_that(bowling_game_score(), is_equal_to(20));
}
