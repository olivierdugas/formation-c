#!/bin/sh
set -e

cd "$(dirname "$0")/../"
. "./scripts/common"

# Creating necessary files and configs to embed the host user in containers.
# The generated user will have its home directory in CONTAINER_USER_HOME
CONTAINER_USER_HOME="${HOME}" . ./scripts/_user-setup

# docker compose commands will automatically build the image if it does
# not exist, but will not detect changes made to it once built.
# Running build here ensures that the environment is up to date when
# you run init.sh
docker compose build

# install project dependencies here as needed

